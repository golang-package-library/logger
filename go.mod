module gitlab.com/golang-package-library/logger

go 1.17

require (
	gitlab.com/golang-package-library/env v0.0.0-20220522085307-1f1e68f337d0
	go.uber.org/zap v1.21.0
)

require (
	go.uber.org/atomic v1.7.0 // indirect
	go.uber.org/multierr v1.6.0 // indirect
)
